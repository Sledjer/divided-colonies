name="Divided Colonies"
archive="mod/dividedcolonies.zip"
tags={
	"Expansion"
	"Gameplay"
	"Map"
	"Historical"
}
picture="DividedColonies.png"
supported_version="1.21.*.*"
